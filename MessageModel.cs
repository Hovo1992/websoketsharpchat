﻿using Newtonsoft.Json;

namespace ChatServer
{
    [JsonObject]
    public class MessageModel
    {
        [JsonProperty("MessageType")]
        public int MessageType { get; set; }

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("SenderName")]
        public string SenderName { get; set; }

        [JsonProperty("SenderSessionId")]
        public string SenderSessionId{ get; set; }
    }
}
