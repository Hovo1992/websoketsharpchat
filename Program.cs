﻿using System;
using WebSocketSharp.Server;

namespace ChatServer
{
    class Program
    {
        const string _host = "ws://localhost:8080";

        static void Main()
        {
            WebSocketServer ws = new WebSocketServer(_host);

            ws.AddWebSocketService<ServerChat>("/ServerChat");

            ws.Start();
            Console.WriteLine("Server started!");

            Console.ReadKey(true);
        }
    }
}
