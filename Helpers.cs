﻿using Newtonsoft.Json;

namespace ChatServer
{
    public static class Helpers
    {
        public static string SerializeObject(object data) => JsonConvert.SerializeObject(data);        
    }
}
