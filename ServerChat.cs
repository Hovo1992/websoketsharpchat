﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using WebSocketSharp;
using WebSocketSharp.Net;
using WebSocketSharp.Server;

namespace ChatServer
{
    public class ServerChat : WebSocketBehavior
    {
        #region Private Fields

        private readonly Dictionary<MessageType, Action<MessageModel>> _receivers;

        #endregion

        #region Properties

        private Cookie UserInfo 
        {
            get
            {
                IWebSocketSession session = null;
                Sessions?.TryGetSession(ID, out session);
                return session.Context.CookieCollection["UserInfo"];
            }
        }

        #endregion

        #region Constructor

        public ServerChat()
        {
            _receivers = new Dictionary<MessageType, Action<MessageModel>>
            {
                { MessageType.Message, ForMessage },
            };
        }

        #endregion

        #region Receiver Actions

        private void ForMessage(MessageModel messageModel)
        {
            var data = new MessageModel()
            {
                MessageType = (int)MessageType.Message,
                Message = messageModel.Message,
                SenderSessionId = ID,
                SenderName = UserInfo.Value
            };

            BroadcastMessage(data);
        }

        #endregion

        #region Overrides

        protected override void OnMessage(MessageEventArgs e)
        {
            var receivedMessageModel = JsonConvert.DeserializeObject<MessageModel>(e.Data);

            _receivers[(MessageType)receivedMessageModel.MessageType].Invoke(receivedMessageModel);
        }

        protected override void OnOpen()
        {
            Console.WriteLine("ID: {0}", ID);
            
            var data = new MessageModel
            {
                MessageType = (int)MessageType.NewConnectedName,
                SenderName = UserInfo.Value,
                SenderSessionId = ID                
            };

            BroadcastNewId(data);
            ProvideAllIdsToNewConnection();
        }

        protected override void OnClose(CloseEventArgs e) => Console.WriteLine($"Client closed {0}" + e.Reason);

        protected override void OnError(ErrorEventArgs e) => Console.WriteLine(e.Exception.Message);

        #endregion

        #region Serve Functions

        private void BroadcastNewId(MessageModel messageModel)
        {
            var jsonRes = Helpers.SerializeObject(messageModel);

            foreach (var id in Sessions.IDs)
                if (id != ID && Sessions.PingTo(id))
                    Sessions.SendTo(jsonRes, id);
        }

        private void ProvideAllIdsToNewConnection()
        {
            foreach (var session in Sessions.Sessions)
                if(Sessions.PingTo(ID) && session.ID != ID)
                {
                    var nameData = new MessageModel
                    {
                        SenderName = session.Context.CookieCollection["UserInfo"].Value,
                        MessageType = (int)MessageType.NewConnectedName,
                        SenderSessionId = ID
                    };

                    Send(Helpers.SerializeObject(nameData));
                }
        }

        private void BroadcastMessage(MessageModel messageModel)
        {
            var jsonRes = Helpers.SerializeObject(messageModel);

            foreach (var id in Sessions.IDs)
                if (Sessions.PingTo(id))
                    Sessions.SendTo(jsonRes, id);
        }

        #endregion
    }
}